package com.verification;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.verification.interfaces.UserDao;


public class Verificator {
	
    public static final ApplicationContext aspectContext = new ClassPathXmlApplicationContext(new String[] { "aspects.xml" }); 
    public static final ApplicationContext appContext = new ClassPathXmlApplicationContext(new String[] { "context.xml" }); 
    

    public static void main(String[] args) {
        String []beans = appContext.getBeanNamesForType(User.class);
        User user;
       
        for(int i =0; i< beans.length; i ++ ) {
            user = (User )appContext.getBean(beans[i]);
            if(createUser(user)){
                System.out.println("Usuario valido: \n"+ user);
            }
            else {
                System.out.println("Error en validacion: \nid:"+ beans[i]+"\n"+user);
            }

        }
        
    }

    private static boolean createUser(User user) {
        UserDao userVerifier = (UserDao) aspectContext.getBean("userDao");
        try {
            //Se llamara hara la validacion antes de crear un nuevo usuario
            userVerifier.createUser(user);
            return true;
        } catch (FailedValidationException exception) {
            //no se cumple la verificacion
            System.out.println(exception.getFriendlyMessage());
            return false;
        }
        
    }
}
