package com.verification.interfaces;

import java.lang.annotation.Annotation;

import com.verification.FailedValidationException;

public interface IVerifier {
    public void validate(Object param, Annotation annotation, Annotation methodAnnotation) throws FailedValidationException;
}
