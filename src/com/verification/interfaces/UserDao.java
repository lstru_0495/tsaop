package com.verification.interfaces;

import com.verification.FailedValidationException;
import com.verification.ValidationPolicy;
import com.verification.User;
import com.verification.annotations.CheckObject;
import com.verification.annotations.Validated;


public interface UserDao {
	@Validated(policy = ValidationPolicy.ADD)
	void createUser(@CheckObject User user) throws FailedValidationException;
        
        @Validated(policy = ValidationPolicy.DELETE)
        void deleteUser(@CheckObject User user) throws FailedValidationException;
}
