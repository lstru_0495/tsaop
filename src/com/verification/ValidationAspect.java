package com.verification;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

import com.verification.validators.ArgumentsValidator;

@Aspect
public class ValidationAspect {
    // Se hace la validacion antes de crear el usuario
    @Before("execution(* com.verification.UserDaoImpl.createUser(..))")
    public void validateBefore(JoinPoint joinPoint) throws Exception {
        MethodSignature mSignature = (MethodSignature) joinPoint.getSignature();
        Object[] args = joinPoint.getArgs();
        try {
            
            ArgumentsValidator.validate(mSignature.getMethod(), args);
        } catch (FailedValidationException exception) {
            throw exception;
        }
    }    
}
