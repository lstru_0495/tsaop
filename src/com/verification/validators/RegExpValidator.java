package com.verification.validators;

import java.lang.annotation.Annotation;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.verification.FailedValidationException;
import com.verification.ValidationPolicy;
import com.verification.annotations.CheckRegexp;
import com.verification.annotations.Validated;
import com.verification.interfaces.IVerifier;

public class RegExpValidator implements IVerifier {

    @Override
    public void validate(Object param, Annotation annotation, Annotation methodAnnotation) throws FailedValidationException {
        CheckRegexp regexpAnnotation = (CheckRegexp) annotation;
        ValidationPolicy policy;

        if (methodAnnotation != null)  {
            policy = ((Validated) methodAnnotation).policy();
        } else {
            policy = ValidationPolicy.ADD;
        }

        for (int i = 0; i < regexpAnnotation.policy().length; i++) {
            if (policy.equals(regexpAnnotation.policy()[i])) {
                Pattern pattern = Pattern.compile(regexpAnnotation.expression());
                Matcher matcher = pattern.matcher((String) param);
                if (!matcher.matches()) {
                    throw new FailedValidationException(regexpAnnotation.messageRef(), "Error en validacion");
                }
                break;
            }
        }
    }
}
