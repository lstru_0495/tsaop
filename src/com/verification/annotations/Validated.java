package com.verification.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import com.verification.ValidationPolicy;

@Retention (RetentionPolicy.RUNTIME)
public @interface Validated {
	ValidationPolicy policy() default ValidationPolicy.ADD;
}
