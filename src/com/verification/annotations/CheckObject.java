package com.verification.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import com.verification.ValidationPolicy;

@Retention (RetentionPolicy.RUNTIME)
public @interface CheckObject {
    //Clase que representa objeto que sera verificado,
    // en este caso seran los usuario
	String friendlyGroup() default "";
	ValidationPolicy policy() default ValidationPolicy.ADD;
}
