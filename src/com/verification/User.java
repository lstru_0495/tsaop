package com.verification;

import com.verification.annotations.CheckRegexp;

public class User {

	@CheckRegexp(expression = "[A-Za-z]{2,}", 
                messageRef = "error_validate_firstName", policy = ValidationPolicy.ADD)
	private String firstName;

	@CheckRegexp(expression = "[A-Za-z]{2,}", 
                messageRef = "error_validate_lastName", policy = ValidationPolicy.ADD)
	private String lastName;
	
        
        //^[^@]+@[^@]+\.[a-zA-Z]{2,}$
        @CheckRegexp(expression = "^[^@]+@[^@]+\\.[a-zA-Z]{2,}$",
                messageRef = "error_validate_email", policy = ValidationPolicy.ADD)

	//@CheckRegexp(expression = "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+" + "(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})",
                //messageRef = "error_validate_email", policy = ValidationPolicy.ADD)
	private String email; 
        
        @CheckRegexp(expression = "^[9][0-9]{8}$", 
                messageRef = "error_validate_phone", policy= ValidationPolicy.ADD)
        private String phone;
	
        @CheckRegexp(expression = "^\\d{5}(?:[-\\s]\\d{4})?$", 
                messageRef = "error_validate_zip", policy= ValidationPolicy.ADD)
        private String zip;

    public String getZip() {
        return zip;
    }

    public void setZip(String zipCode) {
        this.zip = zipCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }      

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    @Override
    public String toString() {
        return "Name: "+this.firstName+" "+this.lastName+"\nPhone: "+
                this.phone+"\nEmail: "+this.email+"\nZip: "+this.zip+"\n";
    }
}
