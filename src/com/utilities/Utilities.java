package com.utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;
import javax.swing.JOptionPane;

public class Utilities {
    public String dbms;  
    public String jarFile;
    public String dbName; 
    public String userName;
    public String password;
    public String urlString;
    
    
    private String driver;
    private Properties prop;
    
    private void setProperties(String fileName) throws FileNotFoundException,
            IOException,InvalidPropertiesFormatException {
        this.prop = new Properties();
        FileInputStream fis = new FileInputStream(fileName);
        prop.loadFromXML(fis);
        
        //LOCAL
        this.dbms = this.prop.getProperty("dbms");
        this.driver = this.prop.getProperty("driver");
        this.dbName = this.prop.getProperty("database_name");
        this.userName = this.prop.getProperty("user_name");
        this.password = this.prop.getProperty("password");
    }
    
    public Connection getConnection () throws SQLException{
        String JDBC = "jdbc";
        Connection connection = null;
        Properties connProps =  new Properties ();
        //LOCAL
        connProps.put("user", this.userName);
        connProps.put("password",this.password);
        this.urlString =  JDBC+":"+this.dbms+":"+this.dbName+";create=true";
        connection = DriverManager.getConnection(this.urlString, connProps);
        System.out.println("Connection \n"+this.urlString);
        return connection;
    }
    
    public Utilities(String propertiesFileName) throws FileNotFoundException, 
            IOException,InvalidPropertiesFormatException{
        this.setProperties(propertiesFileName);   
    }
    
    public static boolean existTable(Connection connection ,String tableName) throws SQLException {
        DatabaseMetaData dbmd = connection.getMetaData();
        ResultSet rs = dbmd.getColumns(null, null, tableName, null); 
        if (rs.next()) {return true;}
        return false;
    }
    
    public static void dropTable (Connection conn, String tableName) throws SQLException {
        String dropString ="drop table "+tableName;
        Statement stmt = null;
        try {
            stmt =conn.createStatement();
            stmt.executeUpdate(dropString);
        }catch(SQLException e){
            System.out.println("No se pudo eliminar la tabla "+tableName);
            System.out.println(e);
        }
        finally {if(stmt != null) {stmt.close();}} 
    }
    
    public static void backUpDatabase(Connection conn)throws SQLException{
        String backupdirectory =".\\backup\\";
        CallableStatement cs = conn.prepareCall("CALL SYSCS_UTIL.SYSCS_BACKUP_DATABASE(?)"); 
        cs.setString(1, backupdirectory);
        cs.execute(); 
        cs.close();
        JOptionPane.showMessageDialog(null,"COPIA DE SEGURIDAD EN "+backupdirectory ,null, JOptionPane.INFORMATION_MESSAGE,null);
        //System.out.println("backed up database to "+backupdirectory);
    }
}
