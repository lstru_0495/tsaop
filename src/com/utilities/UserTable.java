package com.utilities;

import com.verification.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class UserTable { 
    public static final String TABLE_NAME = "USER";
    
    public static void createTable (Connection conn)  {    
        String createString =
            "create table "+TABLE_NAME+" (USER_ID varchar(10) NOT NULL, "+
                " FIRSTNAME varchar(30) NOT NULL, "+
                " LASTNAME varchar(30) ,"+
                " EMAIL varchar(30) ,"+
                " PHONE varchar(10),"+
                " ZIP varchar(8),"+
                " PRIMARY KEY (USER_ID))";
        Statement stmt = null;
        try {
            stmt =conn.createStatement();
            stmt.executeUpdate(createString);
        }catch(SQLException e){
            System.out.println("ERROR AL CREAR TABLA USER" +e);
            System.out.println(e);
        }
        finally {
            try {
                if(stmt != null) {stmt.close();}
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
    
    public static void insertBook(Connection conn, String id, User newUser) { 
        try {
            String insertString = "";
            insertString += "INSERT INTO "+TABLE_NAME;
            insertString += "(USER_ID, FIRSTNAME, LASTNAME, EMAIL, PHONE, ZIP) ";
            insertString += "VALUES ( ";
            insertString += "?, ?, ?, ?, ?, ?";
            insertString += ")";
            System.out.println(insertString);
            PreparedStatement stmt = conn.prepareStatement(insertString);
            stmt.setString(1, id);
            stmt.setString(2, newUser.getFirstName());
            stmt.setString(3, newUser.getLastName());
            stmt.setString(4, newUser.getEmail());
            stmt.setString(5, newUser.getPhone());
            stmt.setString(6, newUser.getZip());
        }catch(SQLException e){
            System.out.println("ERROR AL INSERTARs "+e);
        }
    }
    
}
